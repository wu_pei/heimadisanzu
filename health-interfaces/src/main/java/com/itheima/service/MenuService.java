package com.itheima.service;

import com.itheima.pojo.Menu;

import java.util.List;

public interface MenuService {
    List<Menu> getAllMenus();

    void addOneMenu(Menu menu);

    void delMenuById(Integer id);

    boolean isBindMenu(Integer id);

    Menu findMenuById(Integer id);

    void updateMenuById(Menu menu);
}
