package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.MyUser;
import com.itheima.pojo.User;

import java.util.List;
import java.util.Map;

public interface UserService {
    List<MyUser> findUsers();

    User findUserByUserName(String username);

    Map getMenusAndPermissions();

    PageResult findUsersByCondition(QueryPageBean queryPageBean);

    boolean findUserByUsername(String username);

    void addUser(User user);

    void deleUserById(Integer id);

    User findUserById(Integer id);

    void updateUser(User user);

}
