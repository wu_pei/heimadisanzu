package com.itheima.service;

import com.itheima.entity.LogEntity;

/**
 * @author hong
 */
public interface LogService {
    void saveLog(LogEntity log);
}
