package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Permission;

public interface AuthorityService {
    PageResult findAuthorities(QueryPageBean queryPageBean);

    boolean findPermissionByKeyword(String keyword);

    void addPermission(Permission permission);

    boolean isBindPermission(Integer id);

    void delAuthorityById(Integer id);

    Permission findAuthorityById(Integer id);

    void updateAuthorityById(Permission permission);
}
