package com.itheima.service;

import com.itheima.pojo.OrderSetting;

import java.util.List;

public interface OrderSettingService {
    void setNumberByDate(OrderSetting orderSetting);

    void batchAdd(List<OrderSetting> orderSettings);

    List<OrderSetting> findOrderSettingsByMonth(String month);
}
