package com.itheima.service;

import com.itheima.pojo.Menu;

import java.util.List;

public interface MainService {
    List<Menu> getMenus(String userName);
}
