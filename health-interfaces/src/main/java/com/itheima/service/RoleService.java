package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Role;

import java.util.Map;
import java.util.Set;


public interface RoleService {
    Set<Role> findAllRoles();

    Map findAllPermissionsAndMenus();

    PageResult findRolesByCondition(QueryPageBean queryPageBean);

    boolean findRoleByKeyword(String keyword);

    void addRole(Role role);

    void deleRoleById(Integer id);

    Role findRoleById(Integer id);

    void updateRole(Role role);
}
