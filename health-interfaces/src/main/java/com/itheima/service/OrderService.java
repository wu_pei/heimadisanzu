package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Member;
import com.itheima.pojo.OrderInfoVo;

import java.util.List;
import java.util.Map;

public interface OrderService {
    Result submit(OrderInfoVo orderInfoVo);

    Map findById(Integer id);

    List<Member> findFamilyById(Integer id);

    Result addFamily(Member member1);

    PageResult findPage(QueryPageBean queryPageBean);

}
