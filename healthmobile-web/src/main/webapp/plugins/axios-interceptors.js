// 添加请求拦截器
//在发送请求之前，给每一个页面的header添加token
axios.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    //console.log("在发送请求之前做些什么:");
    //判断是否存在token，除了登录页，其他页面header都添加token
    config.headers.token = localStorage.getItem("token");
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
//发送请求之后，若返回401，则进行处理
axios.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    if(response.data.message === "401"){
        //若返回401，清除token信息并跳转到登录界面
        localStorage.removeItem("token");
        window.location.href = "login.html";
    }
    return response;
}, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
});