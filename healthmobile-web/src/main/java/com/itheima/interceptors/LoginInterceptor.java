package com.itheima.interceptors;

import com.alibaba.fastjson.JSON;
import com.itheima.entity.Result;
import com.itheima.pojo.Member;
import com.itheima.wechat.JedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author ZZH
 * data 2019.10.30 下午 3:47
 */
public class LoginInterceptor implements HandlerInterceptor {
    @Autowired
    JedisUtil jedisUtil;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)  {
        //校验用户是否登录

        //从Header中获取token
        //通过axios的拦截器，我们在每一个请求的Header中添加了token
        String token = request.getHeader("token");
        //未登录
        if(token == null || token == "" || token.equals("null")){
            //401 未授权
            //服务器响应给浏览器
            write4Login(response);
            return false;
        }
        //从redis获取用户登录信息
        String s = jedisUtil.get(token);
        Member member = JSON.parseObject(s, Member.class);
        if(null == member){
            //服务器响应给浏览器
            write4Login(response);
            return false;
        }
        return true;
    }



    private void write4Login(HttpServletResponse response)  {
        try {
            Result result = new Result(false,"401");
            response.setContentType("application/json;charset=utf-8");
            PrintWriter writer = response.getWriter();
            writer.print(JSON.toJSONString(result));
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
