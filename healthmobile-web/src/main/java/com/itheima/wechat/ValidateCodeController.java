package com.itheima.wechat;

import cn.hutool.core.lang.Assert;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.itheima.constant.RedisConstant;
import com.itheima.entity.Result;
import com.itheima.pojo.Params;
import com.itheima.utils.SmsUtil;
import com.itheima.utils.ValidateCodeUtils;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisPool;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;

@RestController
@RequestMapping("/validateCode")
public class ValidateCodeController {

    @Autowired
    JedisUtil jedisUtil;
    @Autowired
    JedisPool jedisPool;

    @RequestMapping("/send4Order")
    public Result send4Order(@RequestBody Params params){
        //校验图形验证码
        String imgCode = params.getImgCode ();
        String deviceId = params.getDeviceId ();
        String redisDeviceId = jedisUtil.get (deviceId);
        if (redisDeviceId ==null || !redisDeviceId.equals ( imgCode )) {
            return Result.error ( "图片验证码错误" );
        }
        String telephone = params.getTelephone ();
        try {
            //生成短信验证码
            Integer code = ValidateCodeUtils.generateValidateCode(4);
            //使用短信工具类发送验证码
//        SmsUtil.sendSmsCode(telephone,String.valueOf(code));
            System.out.println("code:" + code);
            //使用redis(用户提交预约时需要校验用户输入的)
            //key=telephone,value=code
            jedisUtil.setex(telephone + RedisConstant.SENDTYPE_ORDER,60 * 60,String.valueOf(code));

            return Result.success("");
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("短信验证码错误");
        }
    }

    @RequestMapping("/send4Login")
    public Result send4Login(String telephone){
        //生成验证码
        Integer code = ValidateCodeUtils.generateValidateCode(4);
        //使用短信工具类发送验证码
//        SmsUtil.sendSmsCode(telephone,String.valueOf(code));
        System.out.println("code:" + code);
        //使用redis(用户提交预约时需要校验用户输入的)
        //key=telephone,value=code
        jedisUtil.setex(ValidateCodeUtils.getLoginSmsCodeRedisKey(telephone),60 * 60,String.valueOf(code));

        return Result.success("");
    }


    @Autowired
    DefaultKaptcha defaultKaptcha;

    @RequestMapping("/code/{deviceId}")
    public void createCode(@PathVariable String deviceId, HttpServletResponse response) throws Exception {
        Assert.notNull(deviceId, "机器码不能为空");
        response.setHeader("Cache-Control", "no-store, no-cache");
        response.setContentType("image/jpeg");
        //生成文字验证码
        String text = defaultKaptcha.createText();
        //生成图片验证码
        BufferedImage image = defaultKaptcha.createImage(text);
        //生成的验证码写入redis
        jedisPool.getResource().setex(deviceId,60*5,text);
        //获取输出流
        ServletOutputStream out = response.getOutputStream();
        //将图片写回浏览器
        ImageIO.write(image, "JPEG", out);
    }

}
