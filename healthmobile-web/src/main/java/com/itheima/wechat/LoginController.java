package com.itheima.wechat;

import cn.hutool.core.lang.UUID;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.itheima.entity.Result;
import com.itheima.pojo.LoginInfoVo;
import com.itheima.pojo.Member;
import com.itheima.service.MemberService;
import com.itheima.utils.ValidateCodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/login")
public class LoginController {

    @Reference
    MemberService memberService;

    @Reference
    JedisUtil jedisUtil;

    @RequestMapping("/check")
    public Result check(@RequestBody LoginInfoVo loginInfoVo, HttpServletRequest request){
        String telephone = loginInfoVo.getTelephone();
        String validateCode = loginInfoVo.getValidateCode();
//        2.3、从redis得到发送验证码时存入验证码
        String redisCode = jedisUtil.get(ValidateCodeUtils.getLoginSmsCodeRedisKey(telephone));
//        2.4、比对验证码
        if(null == redisCode || !redisCode.equals(validateCode)){
             return Result.error("验证码错误");
        }
//        2.5、先根据手机号码查询当前手机号码是否已经注册
        Member member =  memberService.findByTelephone(telephone);
        if(null == member){
            member = new Member();
            member.setPhoneNumber(telephone);
            member.setRegTime(new Date());
            member = memberService.add(member);//此时就有会员id
        }
//        2.6、把用户登录信息保存在session
//        request.getSession().setAttribute("userInfo", member);
//        Map<String, Map<String,String>> map = new HashMap<>();

        String token = UUID.randomUUID().toString();

        jedisUtil.set(token,JSON.toJSONString(member));

        return Result.success("登录成功",token);
    }


}
