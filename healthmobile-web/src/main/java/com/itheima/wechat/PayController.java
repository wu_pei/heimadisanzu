package com.itheima.wechat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.itheima.pojo.Member;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@RestController
@RequestMapping("/pay")
public class PayController {
    Logger log = LoggerFactory.getLogger(PayController.class);
    String serverUrl = "https://openapi.alipaydev.com/gateway.do";
    String APP_ID = "2016101700707414";
    String APP_PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDUL5bpXfw2k7I25Rz3oE6TtQkNURUKmiWcZW+QY1Na+5PfEXFLKg0iHpN/z7rMIhfBMpzYzhBuDP7+Id/8QoKa250QngI6eVf5Y8qxoqzMFntG6X9F+nu+/lWHetsrogb86KTGLz3GFQ/LpUKylylOxHt6BAMCKGs839CktzfBx7SyDvZD4nO15Xc/WxX6I9rTRYPUUe+7OjQoLeXRJAeP5cnpQU8dxsm21jaLfcXMFhkhkVpuZXvlD3tLxpRfAoUjLv7OmI8Q2QMAsOkhbtnd44l30pzKveFyqQuC7IZX1zV3eHIle8o4ovBb4aOFUC+53sB2MQESm9vkU5AtFJ5FAgMBAAECggEBAKUhQCXvscfbm7cjCS8hf+Vt31SQxHpSynBzdKZ+1jEZVtQkI/JmEIB8ITg3tt0Er034kJ4diCXNUWj/few46qGPxiVXgs50/uUkJ2uk+St/V4MxVo5kSIe0MWXVU9oaJ6wNdaaSv1V3sKpAu652+3vEZ3/mvVyhjgt690opGGhiEk+v/uaigbM3XMFcpOJBa/XVYkGXvBfU8t1lC1st8tOog4QG7B83shW3LZ0TdbkOh+vMb3d1TGBYU3LyYfbLlpN/52iqfj0QyJnFGWETHYlE77dOixJLNv1EF9aSKgVQJ5SsVcnK2VeXyCyDvCTicZHX64smjgQ9MaZAb/UpNqECgYEA+04ovGZ7f9Q3nD0lb+3YXT6U5vJmolX9saxN17rCORNEGalVf5gzUa/XajFwvkY6rjWROVfNOPmoBeRhA4AlnbNX/lrFPkpV2h+2f9DaCfBRiPvRoGcuUlhB3WxnI1FXYE5e92/kqyu1Uomb/4iufp6fUkY0yQgM5BcfaKe3oZ0CgYEA2CZYkCI1ugFYfLebAy1GdxMTD6jDqAa+GY720Ff/fP7JIma1S42l5KsUkLsovz3/1TgfIdjCDZN6MRRjYL/gPCmVBfsZ98UqxCiUhgtgp0orU7A1WwI/+zyeiZoNV4s4GOCAKDdfGZ4NCyfToOwFDo9AbWTfjDnnkw/N9xOZgskCgYEAgnxx4GliJNyfR2vkfXrFRp1RGZ2xtwbpZJDwFlztrk8XpC9tqoPv2bFxK72iKdoza/N6hYjWkYfoO676xKOMZJcT0Il3M0HL1Cd1Tma2YES9QWBn117cCXu0L3GzF/6LXQE0hExAMrTOQJ4dNxDUcgCQ/1pe3Cdm8iQjdeboDW0CgYBs13pXMfLN9CmDu8zh36WRBljd1MSZUrPdGOFt7jHn1kCgAwodvnkf2YokiyosiuLNklIS8BxfS7kHB9Y2/MfEY8f32AaufmlXl3uauBX2h2Gj5aIobsybDV5jueAoCiG4XSwLlD5eHvlAedCWQuMU/suZimcLLIlQw2+akBMICQKBgAPiIPkZsaBzkFBwZzA+47Bl93LP5XHAHQ+dMmpOtREXccEbF6VLzEexJgGpJ+NckSep7B2q4SvbOEEe5KJYfScvdoqXt+5ZKVfm56+FgEiwh8hzHR8FmV09NCktWkK8liGIX0PVuHbcQ/Qvq48QB9yeJ/rBQUwLTXDP/z3UtI6P";
    String CHARSET = "utf-8";
    String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAn/YmRfxaz1K9PaC6te5/Q7KKzjZqkN935iwFAbZ5pcAjrcHd1+p5SSvKUiV3evtqDbbvCQ5jJBdudpa1mpqlpyZiuKPfFdhX4G9DZ0aDsY0VVdAb43DqK1G1C5cjXGtU7fv0rdAS9FyCXh0YA/CBdj+zMMTvzf9Gu8EgOJnzJXFOKF1Ugo1JadAlIooUTyz49FqhZULNC33BL+OzXvubmNHxaVvJ/4x6IL3SgmteQ7Hi47zMtZwRljvWV6kEBy5LpmRRdmhr5IwsVjizukY7BBB90mCRqNI74meAvaXpbCLadvIVDFXWGxbuWgfCCqKFuD07gHJuny1ubLy/wyt9NQIDAQAB";

    @RequestMapping("/alipay")
    public void alipay(HttpServletRequest httpRequest,
                       HttpServletResponse httpResponse) throws ServletException, IOException {

        AlipayClient alipayClient = new DefaultAlipayClient(serverUrl,
                APP_ID, APP_PRIVATE_KEY,
                "json", CHARSET,
                ALIPAY_PUBLIC_KEY, "RSA2"); //获得初始化的AlipayClient

        AlipayTradeWapPayRequest alipayRequest = new AlipayTradeWapPayRequest();//创建API对应的request
        alipayRequest.setReturnUrl("http://kebh58.natappfree.cc/pages/paysuccess.html");//支付成功之后跳转到哪里
        alipayRequest.setNotifyUrl("http://kebh58.natappfree.cc/pay/alipayNotify.do");//支付成功之后的通知地址
//        alipayRequest.setBizContent("{" +
//                " \"out_trade_no\":\"20150320010101002\"," +
//                " \"total_amount\":\"88.88\"," +
//                " \"subject\":\"Iphone6 16G\"," +
//                " \"product_code\":\"QUICK_WAP_PAY\"" +
//                " }");//填充业务参数


        JSONObject json = new JSONObject();
        json.put("out_trade_no",System.currentTimeMillis());
        json.put("total_amount",5000);
        json.put("subject","洗澡");
        alipayRequest.setBizContent(json.toJSONString());
//        out_trade_no	商户订单号，需要保证不重复
//        subject	订单标题
//        total_amount	订单金额

        String form="";
        try {
            form = alipayClient.pageExecute(alipayRequest).getBody(); //调用SDK生成表单
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        httpResponse.setContentType("text/html;charset=" + CHARSET);
        httpResponse.getWriter().write(form);//直接将完整的表单html输出到页面
        httpResponse.getWriter().flush();
        httpResponse.getWriter().close();
    }


    @RequestMapping(value = "/alipayNotify")
    @ResponseBody
    public String alipayNotify(HttpServletRequest request, HttpServletRequest response) throws Exception {

        log.info("支付成功, 进入异步通知接口...");

        //获取支付宝POST过来反馈信息
        Map<String,String> params = new HashMap<String,String>();
        Map<String,String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用
//			valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }
        boolean signVerified = AlipaySignature.rsaCheckV1(params, ALIPAY_PUBLIC_KEY, "utf-8", "RSA2"); //调用SDK验证签名
        //——请在这里编写您的程序（以下代码仅作参考）——
		/* 实际验证过程建议商户务必添加以下校验：
		1、需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
		2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
		3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
		4、验证app_id是否为该商户本身。
		*/
        if(signVerified) {//验证成功
            //商户订单号
            String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"),"UTF-8");

            //支付宝交易号
            String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"),"UTF-8");

            //交易状态
            String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"),"UTF-8");

            //付款金额
            String total_amount = new String(request.getParameter("total_amount").getBytes("ISO-8859-1"),"UTF-8");

            if(trade_status.equals("TRADE_FINISHED")){
                //判断该笔订单是否在商户网站中已经做过处理
                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                //如果有做过处理，不执行商户的业务程序

                //注意： 尚自习的订单没有退款功能, 这个条件判断是进不来的, 所以此处不必写代码
                //退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知
            }else if (trade_status.equals("TRADE_SUCCESS")){
                //判断该笔订单是否在商户网站中已经做过处理
                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                //如果有做过处理，不执行商户的业务程序

                //注意：
                //付款完成后，支付宝系统发送该交易状态通知

                // 修改叮当状态，改为 支付成功，已付款; 同时新增支付流水
            }
            log.info("支付成功...");
        }else {//验证失败
            log.info("支付, 验签失败...");
        }

        return "success";
    }


}
