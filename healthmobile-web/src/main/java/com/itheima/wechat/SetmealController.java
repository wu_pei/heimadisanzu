package com.itheima.wechat;

import cn.hutool.core.lang.UUID;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.itheima.constant.RedisCache;
import com.itheima.entity.Result;
import com.itheima.interceptors.CurrentUser;
import com.itheima.pojo.Member;
import com.itheima.pojo.Setmeal;
import com.itheima.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/setmeal")
public class SetmealController {

    @Autowired
    JedisUtil jedisUtil;
    @Reference
    SetmealService setmealService;

    @RequestMapping("/getSetmeal")
    public Result getSetmeal(Integer pageSize, Integer pageNum, HttpServletRequest request){
//        String token = request.getHeader("token");
//
//        String json = jedisUtil.get(token);
//
//        Member member = JSON.parseObject(json, Member.class);
//
//        if(null == member){
//            return Result.error("401");
//        }
        //使用redis缓存
        String getSetmeal = jedisUtil.get(RedisCache.GETSETMEAL);
        if (null == getSetmeal) {
            List<Setmeal> setmeals = setmealService.getSetmeal();
            getSetmeal = JSON.toJSONString(setmeals);
            jedisUtil.setex(RedisCache.GETSETMEAL, 30 * 60,getSetmeal);
        }
        List<Setmeal> setmeals = JSON.parseArray(getSetmeal, Setmeal.class);

        return Result.success("",setmeals);
    }

    @RequestMapping("/findById")
    public Result findById(Integer id,@CurrentUser Member member){
        //使用redis缓存
        String findById = jedisUtil.get(RedisCache.FINDBYID);
        if (null == findById) {
            Setmeal setmeal = setmealService.findByIdBatch(id);
            findById = JSON.toJSONString(setmeal);
            jedisUtil.setex(RedisCache.FINDBYID, 30 * 60, findById);
        }
        Setmeal setmeal = JSON.parseObject(findById, Setmeal.class);

        return Result.success("",setmeal);
    }

    @RequestMapping("/findDetailById")
    public Result findDetailById(Integer id,@CurrentUser Member member){
        //使用redis缓存
        String findDetailById = jedisUtil.get(RedisCache.FINDDETAILBYID);
        if (null == findDetailById) {
            Setmeal setmeal = setmealService.findDetailById(id);
            findDetailById = JSON.toJSONString(setmeal);
            jedisUtil.setex(RedisCache.FINDDETAILBYID, 30 * 60, findDetailById);
        }
        Setmeal setmeal = JSON.parseObject(findDetailById, Setmeal.class);
        return Result.success("",setmeal);
    }

    @RequestMapping("/getToken")
    public Result getToken(){
        String token = UUID.randomUUID().toString();
        jedisUtil.setex(token,30 * 60,token);
        return Result.success("",token);
    }




    public static void main(String[] args) {
        String url = "http://localhost:84/pages/setmeal_detail.html?id=5&name=jack&age=19";
        //["http://localhost:84/pages/setmeal_detail.html","id=5&name=jack&age=1"]

        String id = getParam(url, "id");
        String name = getParam(url, "name");
        String age = getParam(url, "age");
        System.err.println(id);
        System.err.println(name);
        System.err.println(age);
    }


    private static String getParam(String url,String key){
        String[] urlArrys = url.split("\\?");

        //["id=5","name=jack","age=1"]
        String[] params = urlArrys[1].split("&");


        for (String param : params) {
            String[] p = param.split("=");
            String keyTemp = p[0];
            String val = p[1];
            if(keyTemp.equals(key)){
              return val;
            }
        }

        return "";
    }



}
