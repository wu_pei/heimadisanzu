package com.itheima.wechat;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.itheima.constant.RedisConstant;
import com.itheima.entity.Result;
import com.itheima.interceptors.CurrentUser;
import com.itheima.pojo.Member;
import com.itheima.pojo.Order;
import com.itheima.pojo.OrderInfoVo;
import com.itheima.service.OrderService;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    JedisUtil jedisUtil;

    @Reference
    OrderService orderService;

    @RequestMapping("/submit")
    public Result submit(@RequestBody OrderInfoVo orderInfoVo){

        /**String sign = SignatureUtil.sign(orderInfoVo);
        Long incr = jedisUtil.incr(sign);//把key的value +1 （redis是单线程的）
        if(1 != incr){
            return Result.error("非法请求");
        }*/

        /**String token = orderInfoVo.getToken();
        Long delResult = jedisUtil.del(token);//redis是单线程的，删除的的请求需要排队
        if(delResult != 1){
            return Result.error("非法请求");
        }*/

        String validateCode = orderInfoVo.getValidateCode();
        String telephone = orderInfoVo.getTelephone();
//        1.3.1、校验验证码（拿用户输入的验证码和点击发送验证码时存入redis的验证码比对）
        String redisCode = jedisUtil.get(telephone + RedisConstant.SENDTYPE_ORDER);
        //要么没发，要么过期了
        if(null == redisCode || !redisCode.equals(validateCode)){
            return Result.error("验证码错误");
        }

        //根据用户id查询收货地址

        //预约类型是微信
        orderInfoVo.setOrderType(Order.ORDERTYPE_WEIXIN);

        Result result = orderService.submit(orderInfoVo);

        if(result.isFlag()){
            System.out.println("预约成功，请于" + orderInfoVo.getOrderDate() + "准时到医院");
        }
        return result;
    }

    @RequestMapping("/findById")
    public Result findById(Integer id){
      Map map = orderService.findById(id);
      return Result.success("",map);
    }


    //查询家庭联系人
    @RequestMapping("/getFamily")
    public Result getFamily(@com.itheima.interceptors.CurrentUser Member member) {
        //根据用户id查询家庭联系人
        List<Member> members = orderService.findFamilyById(member.getId());
        return Result.success("", members);
    }

    //增加联系人
    @RequestMapping("/addFamily")
    public Result addFamily(@com.itheima.interceptors.CurrentUser Member member, @RequestBody Member member1) {
        Integer parentId = member.getId();
        member1.setParentId(parentId);
        Result result = orderService.addFamily(member1);
        return Result.success("", result);
    }



    public static void main(String[] args) {
        OrderInfoVo orderInfoVo = new OrderInfoVo();
        orderInfoVo.setName("112");
        orderInfoVo.setSetmealId(5);
        String sign = SignatureUtil.sign(orderInfoVo);
        System.out.println(sign);
        //eebf1eb35ff384ee652f8251c94efb72
        //eebf1eb35ff384ee652f8251c94efb72
        //eebf1eb35ff384ee652f8251c94efb72
        //4cce8323b7665242595d2719b9d24a44
//        4cce8323b7665242595d2719b9d24a44
    }

}
