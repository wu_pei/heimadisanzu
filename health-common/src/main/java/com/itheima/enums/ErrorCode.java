package com.itheima.enums;

public enum ErrorCode {
    CHECKITEM_ADD_SUCCESS(0,"检查组新增成功"),
    CHECKITEM_ADD_FAIL(-1,"检查组新增成功")
    ;
    private Integer code;
    private String msg;

     ErrorCode(Integer code,String msg){
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
