package com.itheima.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author hong
 */
public class GetLocalIp {
    public static String localIp() {
        try {
            InetAddress tmpAddress = InetAddress.getLocalHost();
            return tmpAddress.getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }
}

