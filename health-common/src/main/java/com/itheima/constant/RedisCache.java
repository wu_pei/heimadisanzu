package com.itheima.constant;

public class RedisCache {
    public static final String GETSETMEAL = "getSetmeal";
    public static final String FINDBYID = "findById";
    public static final String FINDDETAILBYID = "findDetailById";
}
