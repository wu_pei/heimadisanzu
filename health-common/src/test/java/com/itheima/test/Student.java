package com.itheima.test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Student {

    private String name;
    private int age;
    private int code;

    public Student(String name, int age, int code) {
        this.name = name;
        this.age = age;
        this.code = code;
    }

    public static void main(String[] args) {
        Student student = new Student("小名",20,115);
        Student student1 = new Student("小名",24,115);
        Student student2 = new Student("小名",20,115);

        List<Student> studentList = Arrays.asList(student, student1, student2);

        //按照年龄分组
        Map<Integer, List<Student>> ageMap = studentList.stream().collect(Collectors.groupingBy(Student::getAge));
        System.out.println("ageMap = " + ageMap);

        //按照年龄分组，并求出个数
        Map<Integer, Long> ageMap2 = studentList.stream().collect(Collectors.groupingBy(Student::getAge, Collectors.counting()));
        System.out.println("ageMap2 = " + ageMap2);

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
