package com.itheima.test;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import org.junit.Test;

public class qiniu {

    @Test
    public void upload(){
        //指定你新建存储空间选的存储区域
        Configuration cfg = new Configuration(Region.huadong());
        UploadManager uploadManager = new UploadManager(cfg);
        //类似账号
        String accessKey = "3Zy-khB5LooHTnLnGONlS8Nl3yoSMLOzUasXKJAf";
        //类似密码
        String secretKey = "TbBP5xjKWh5ReslyzCz69pPLJDE95TgN9KybdoLK";

        //新建存储空间时输入的名称
        String bucket = "health84";

        String localFilePath = "C:\\Users\\ThinkPad\\Pictures\\q3.jpg";
        //默认不指定key的情况下，以文件内容的hash值作为文件名
        String key = "q33.jpg";

        Auth auth = Auth.create(accessKey, secretKey);
        String upToken = auth.uploadToken(bucket);
        try {
            Response response = uploadManager.put(localFilePath, key, upToken);
            DefaultPutRet defaultPutRet = JSON.parseObject(response.bodyString(), DefaultPutRet.class);
            //解析上传成功的结果
            System.out.println(defaultPutRet.key);
            System.out.println(defaultPutRet.hash);
        } catch (QiniuException ex) {
            Response r = ex.response;
            System.err.println(r.toString());
            try {
                System.err.println(r.bodyString());
            } catch (QiniuException ex2) {
                //ignore
            }
        }
    }

    @Test
    public void delete(){
        //指定你新建存储空间选的存储区域
        Configuration cfg = new Configuration(Region.huadong());

        //类似账号
        String accessKey = "3Zy-khB5LooHTnLnGONlS8Nl3yoSMLOzUasXKJAf";
        //类似密码
        String secretKey = "TbBP5xjKWh5ReslyzCz69pPLJDE95TgN9KybdoLK";

        //新建存储空间时输入的名称
        String bucket = "health84";
        String key = "q3.jpg";
        Auth auth = Auth.create(accessKey, secretKey);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            bucketManager.delete(bucket, key);
        } catch (QiniuException ex) {
            //如果遇到异常，说明删除失败
            System.err.println(ex.code());
            System.err.println(ex.response.toString());
        }
    }

}
