package com.itheima.test;

import java.util.concurrent.*;

public class ThreadTest {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
//        new Callable<Object>(){
//            @Override
//            public Object call() throws Exception {
//                return null;
//            }
//        }

        //固定创建5个线程
        ExecutorService executorService = Executors.newFixedThreadPool(5);

//        Runnable task = new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("111");
//            }
//        };


        Callable<Object> task = new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                Thread.sleep(5000);
                return "1";
            }
        };

        Future<Object>  future = executorService.submit(task);


        System.out.println(future.get());
        System.out.println("执行完了");

        executorService.shutdown();
    }
}
