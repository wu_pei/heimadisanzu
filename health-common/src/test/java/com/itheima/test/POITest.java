package com.itheima.test;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class POITest {

    //XSSF － 提供读写Microsoft Excel OOXML XLSX格式档案的功能
    @Test
    public void write() throws IOException {
//        1、创建excel对象（工作簿）
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook();
//        2、创建sheet（工作表）
        XSSFSheet mysheet = xssfWorkbook.createSheet("mysheet");
//        3、创建行（row）
        XSSFRow row0 = mysheet.createRow(0);
//        4、创建列（cell）并设置cellValue
        row0.createCell(0).setCellValue("姓名");
        row0.createCell(1).setCellValue("爱好");
        row0.createCell(2).setCellValue("地址");


        XSSFRow row1 = mysheet.createRow(1);
        row1.createCell(0).setCellValue("韦小明");
        row1.createCell(1).setCellValue("洗脚，搓澡");
        row1.createCell(2).setCellValue("航头");


        XSSFRow row2 = mysheet.createRow(2);
        row2.createCell(0).setCellValue("凤姐");
        row2.createCell(1).setCellValue("抽烟，喝酒，烫头");
        row2.createCell(2).setCellValue("航头");

//        5、把excel对象写入文件

        FileOutputStream out = new FileOutputStream("C:\\Users\\ThinkPad\\Documents\\test.xlsx");

        xssfWorkbook.write(out);

        out.flush();
        out.close();
        xssfWorkbook.close();
    }

    @Test
    public void read() throws Exception{
        String path = "C:\\Users\\ThinkPad\\Documents\\test.xlsx";

        //1、加载Excel对象（指定一个路径）
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook(path);
        //2、获取sheet
        XSSFSheet mysheet = xssfWorkbook.getSheet("mysheet");
        //3、获取row
//        int lastRowNum = mysheet.getLastRowNum();//最后一行的下标
//        for (int i = 0; i <= lastRowNum; i++) {
//            XSSFRow row = mysheet.getRow(i);
//            int physicalNumberOfCells = row.getPhysicalNumberOfCells();
//            for (int j = 0; j < physicalNumberOfCells; j++) {
//                System.out.print(row.getCell(j).getStringCellValue() + ",");
//            }
//            System.out.println();
//        }
        //4、获取cell并获取value


        for (Row cells : mysheet) {
            for (Cell cell : cells) {
                System.out.println(cell.getStringCellValue());
            }
        }

        xssfWorkbook.close();

    }
}
