package com.itheima.web;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.pojo.RegDate;
import com.itheima.service.MembershipService;
import com.itheima.entity.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/MemberDedail")
public class MembershipController {

    @Reference
    private MembershipService membershipService;

    @RequestMapping("/findMembersByDateRange")
    public Result findMembersByDateRange(@RequestBody RegDate regDate) {
        if (null == regDate.getStartTime()||null == regDate.getEndTime()) {
            return Result.error("没有收到指定时间段");
        }

        /*字符串转换日期*/
        Date date1 = null;
        Date date2 = null;
        try {
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(regDate.getStartTime());
            date2 = new SimpleDateFormat("yyyy-MM-dd").parse(regDate.getEndTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }


        try {
            //计算量日期之间相差的月数
            int monthsDiff = getMonthsDiff(regDate);
            //开始月份
            DateTime dateTime = DateUtil.offsetMonth(date2, -monthsDiff);

            //将月数集合传给service
            List<String> months = new ArrayList<>();
            for (int i = 0; i < monthsDiff; i++) {
                months.add(DateUtil.offsetMonth(dateTime, i).toString("yyyy-MM"));
            }
            List<Integer> counts = membershipService.findMembersByDateRange(months);

            Map<String, Object> map = new HashMap<>();
            map.put("months", months);
            map.put("memberCount", counts);
            return Result.success("回显成功", map);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("回显失败", null);
        }
    }

    private int getMonthsDiff(@RequestBody RegDate regDate) {

        Date date1 = null;
        Date date2 = null;
        try {
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(regDate.getStartTime());
            date2 = new SimpleDateFormat("yyyy-MM-dd").parse(regDate.getEndTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        c1.setTime(date1);
        c2.setTime(date2);
        int year1 = c1.get(Calendar.YEAR);
        int year2 = c2.get(Calendar.YEAR);
        int month1 = c1.get(Calendar.MONTH);
        int month2 = c2.get(Calendar.MONTH);
        int day1 = c1.get(Calendar.DAY_OF_MONTH);
        int day2 = c2.get(Calendar.DAY_OF_MONTH);
        // 获取年的差值 
        int yearInterval = year1 - year2;
        // 如果 d1的 月-日 小于 d2的 月-日 那么 yearInterval-- 这样就得到了相差的年数
        if (month1 < month2 || month1 == month2 && day1 < day2) {
            yearInterval--;
        }
        // 获取月数差值
        int monthInterval = (month1 + 12) - month2;
        if (day1 < day2) {
            monthInterval--;
        }
        monthInterval %= 12;
        return Math.abs(yearInterval * 12 + monthInterval);
    }
}
