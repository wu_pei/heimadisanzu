package com.itheima.web;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Menu;
import com.itheima.pojo.Role;
import com.itheima.service.MenuService;
import com.itheima.service.RoleService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("role")
public class RoleController {
    @Reference
    private RoleService roleService;

    @Reference
    private MenuService menuService;

    @RequestMapping("findAllRoles")
    public Result findAllRoles(){
        try {
            Set<Role> roles = roleService.findAllRoles();
            return Result.success("查询角色成功",roles);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("查询角色失败");
        }
    }

    @RequestMapping("findAllPermissionsAndMenus")
    public Result findAllPermissionsAndMenus(){
        try {
            Map map = roleService.findAllPermissionsAndMenus();
            List<Menu> menus = menuService.getAllMenus();
            map.put("menus", menus);

            return Result.success("查询信息成功",map);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("查询信息失败");
        }
    }

    @RequestMapping("findRolesByCondition")
    public Result findRolesByCondition(@RequestBody QueryPageBean queryPageBean){
        try {
            PageResult pageResult = roleService.findRolesByCondition(queryPageBean);
            return Result.success("查询角色信息成功",pageResult);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("查询角色失败");
        }
    }

    @RequestMapping("addRole")
    public Result addRole(@RequestBody Role role){
        if (!CollectionUtil.isNotEmpty(role.getPermissionIds()) || !CollectionUtil.isNotEmpty(role.getMenuIds())){
            return Result.error("请勾选权限或者菜单");
        }
        boolean flag = roleService.findRoleByKeyword(role.getKeyword());
        if (flag){
            try {
                roleService.addRole(role);
                return Result.success("添加成功");
            } catch (Exception e) {
                e.printStackTrace();
                return Result.error("添加失败");
            }
        }else {
            return Result.error("添加失败,角色关键词不能重复");
        }
    }

    @RequestMapping("deleRoleById")
    public Result deleRoleById(Integer id){
        try {
            roleService.deleRoleById(id);
            return Result.success("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("删除失败");
        }
    }

    @RequestMapping("findRoleById")
    public Result findRoleById(Integer id){
        try {
            Role role = roleService.findRoleById(id);
            List<Menu> menus = menuService.getAllMenus();
            role.setMenus(menus);
            return Result.success("查询成功",role);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("查询失败");
        }
    }

    @RequestMapping("updateRole")
    public Result updateRole(@RequestBody Role role){
        try {
            roleService.updateRole(role);
            return Result.success("更新成功");
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("更新失败");
        }
    }
}
