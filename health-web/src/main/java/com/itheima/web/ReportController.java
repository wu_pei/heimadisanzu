package com.itheima.web;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.entity.Result;
import com.itheima.pojo.HotSetmealVo;
import com.itheima.pojo.ReportDataVo;
import com.itheima.service.ReportService;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

@RestController
@RequestMapping("/report")
public class ReportController {

    @Reference
    ReportService reportService;

    @RequestMapping("/getMemberReport")
    public Result getMemberReport(){
        List<String> months = new ArrayList<>();
        DateTime dateTime = DateUtil.offsetMonth(new Date(), -12);
//        ["2018-11","2018-12","2019-01","2019-02"]
        for (int i = 1; i <= 12; i++) {
            months.add(DateUtil.offsetMonth(dateTime, i).toString("yyyy-MM"));
        }


        List<Integer> memberCount = reportService.getMemberReport(months);
        Map<String,Object> map = new HashMap<>();
        map.put("months",months);
        map.put("memberCount",memberCount);
        return Result.success("",map);
    }

    @RequestMapping("/getSetmealReport")
    public Result getSetmealReport(){
        //[{value:12,name:"xx"},{value:12,name:"xx"}]
        List<Map> data = reportService.getSetmealReport();
        return Result.success("",data);
    }

    @RequestMapping("/getBusinessReportData")
    public Result getBusinessReportData(){
        ReportDataVo reportDataVo = reportService.getBusinessReportData();
        return Result.success("",reportDataVo);
    }


    @RequestMapping("/exportBusinessReport")
    public void exportBusinessReport(HttpServletRequest request,HttpServletResponse response){
        try {
//            String path = request.getSession().getServletContext().getRealPath("/template/report_template.xlsx");
            ReportDataVo reportDataVo = reportService.getBusinessReportData();
            String path = "D:\\template\\report_template.xlsx";
            XSSFWorkbook workbook = new XSSFWorkbook(path);

            XSSFSheet sheetAt = workbook.getSheetAt(0);

            sheetAt.getRow(2).getCell(5).setCellValue(reportDataVo.getReportDate());

            sheetAt.getRow(4).getCell(5).setCellValue(reportDataVo.getTodayNewMember());
            sheetAt.getRow(4).getCell(7).setCellValue(reportDataVo.getTotalMember());

            sheetAt.getRow(5).getCell(5).setCellValue(reportDataVo.getThisWeekNewMember());
            sheetAt.getRow(5).getCell(7).setCellValue(reportDataVo.getThisMonthNewMember());

            sheetAt.getRow(7).getCell(5).setCellValue(reportDataVo.getTodayOrderNumber());
            sheetAt.getRow(7).getCell(7).setCellValue(reportDataVo.getTodayVisitsNumber());
            sheetAt.getRow(8).getCell(5).setCellValue(reportDataVo.getThisWeekOrderNumber());
            sheetAt.getRow(8).getCell(7).setCellValue(reportDataVo.getThisWeekVisitsNumber());
            sheetAt.getRow(9).getCell(5).setCellValue(reportDataVo.getThisMonthOrderNumber());
            sheetAt.getRow(9).getCell(7).setCellValue(reportDataVo.getThisMonthVisitsNumber());


            List<HotSetmealVo> hotSetmeal = reportDataVo.getHotSetmeal();

            int index = 12;
            for (HotSetmealVo hotSetmealVo : hotSetmeal) {
                sheetAt.getRow(index).getCell(4).setCellValue(hotSetmealVo.getName());
                sheetAt.getRow(index).getCell(5).setCellValue(hotSetmealVo.getSetmealCount());
                sheetAt.getRow(index).getCell(6).setCellValue(hotSetmealVo.getProportion().doubleValue());
                index ++;
            }

            ServletOutputStream out = response.getOutputStream();
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setHeader("Content-Disposition",
                    "attachment;filename="+ java.net.URLEncoder.encode("运营数据报表", "UTF-8")+".xlsx");
            workbook.write(out);
            out.flush();
            out.close();
            workbook.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @RequestMapping("getSexAndAgeReport")
    public Result getSexAndAgeReport() {
        List<List<Map>>  data = reportService.getSexAndAgeReport();
        return Result.success("", data);
    }
}
