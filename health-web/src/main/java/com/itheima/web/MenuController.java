package com.itheima.web;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.entity.Result;
import com.itheima.pojo.Menu;
import com.itheima.service.MenuService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("menu")
public class MenuController {
    @Reference
    private MenuService menuService;

    @RequestMapping("getAllMenus")
    public Result getAllMenus() {
        try {
            List<Menu> menus = menuService.getAllMenus();
            return Result.success("查询菜单成功", menus);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("查询菜单失败");
        }
    }

    @RequestMapping("addMenu")
    public Result addOneMenu(@RequestBody Menu menu) {
        try {
            menuService.addOneMenu(menu);
            return Result.success("添加菜单成功");
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("添加菜单失败");
        }
    }

    @RequestMapping("delMenuById")
    public Result delMenuById(Integer id){
        boolean flag = menuService.isBindMenu(id);
        if (flag){
            try {
                menuService.delMenuById(id);
                return Result.success("删除菜单成功");
            } catch (Exception e) {
                e.printStackTrace();
                return Result.error("删除菜单失败");
            }
        }else {
            return Result.error("删除菜单失败,此菜单与角色存在绑定关系");
        }
    }

    @RequestMapping("findMenuById")
    public Result findMenuById(Integer id){
        try {
            Menu menu = menuService.findMenuById(id);
            return Result.success("查询菜单成功", menu);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("查询菜单失败");
        }
    }

    @RequestMapping("updateMenuById")
    public Result updateMenuById(@RequestBody Menu menu){
        try {
            menuService.updateMenuById(menu);
            return Result.success("更新菜单成功");
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("更新数据失败");
        }
    }
}
