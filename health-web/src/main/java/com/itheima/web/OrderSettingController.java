package com.itheima.web;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.entity.Result;
import com.itheima.pojo.OrderSetting;
import com.itheima.service.OrderSettingService;
import com.itheima.utils.POIUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/ordersetting")
public class OrderSettingController {

    @Reference
    OrderSettingService orderSettingService;

    @RequestMapping("/setNumberByDate")
    public Result setNumberByDate(@RequestBody OrderSetting orderSetting){
        orderSettingService.setNumberByDate(orderSetting);
        return Result.success("");
    }

    @RequestMapping("/upload")
    public Result upload(MultipartFile excelFile){
        try {
            //数组里面就是excel每一行的数据
            List<String[]> dataList = POIUtils.readExcel(excelFile);
            List<OrderSetting> orderSettings = new ArrayList<>();
            //把数组里面的数据装换成OrderSetting对象
            if(CollectionUtil.isNotEmpty(dataList)){
                for (String[] row : dataList) {
                    if(row.length != 2){
                        continue;
                    }
                    String dateStr = row[0];//日期
                    String numberStr = row[1];//可预约人数
                    if(StringUtils.isEmpty(dateStr)){
                        continue;
                    }
                    OrderSetting orderSetting = new OrderSetting();
                    orderSetting.setOrderDate(DateUtil.parse(dateStr,"yyyy/MM/dd"));
                    orderSetting.setNumber(Integer.valueOf(numberStr));
                    orderSettings.add(orderSetting);
                }
            }
            orderSettingService.batchAdd(orderSettings);
            return Result.success("");
        } catch (IOException e) {
            e.printStackTrace();
            return Result.error("");
        }
    }
    @RequestMapping("/findOrderSettingsByMonth")
    public Result findOrderSettingsByMonth(String month){
        //{"date":22,"mouth":9,"number":300,"reservations":300}
        List<OrderSetting> orderSettings = orderSettingService.findOrderSettingsByMonth(month);
        for (OrderSetting orderSetting : orderSettings) {
            Date orderDate = orderSetting.getOrderDate();
            orderSetting.setMouth(orderDate.getMonth());
            orderSetting.setDate(orderDate.getDate());
        }

//        List<OrderSetting> orderSettingsNew = new ArrayList<>();
//
//        for (OrderSetting orderSetting : orderSettings) {
//
//            OrderSetting settingNew = new OrderSetting();
//            settingNew.setDate(orderSetting.getOrderDate().getDate());
//        }



        return Result.success("",orderSettings);
    }

}
