package com.itheima.web;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.entity.Result;
import com.itheima.pojo.Menu;
import com.itheima.service.MainService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/main")
public class MainController {
    @Reference
    private MainService mainService;

    @RequestMapping("/getMenus")
    public Result getMenus(String userName) {
        List<Menu> menuList = (List<Menu>) mainService.getMenus(userName);
        return Result.success("", menuList);
    }
}
