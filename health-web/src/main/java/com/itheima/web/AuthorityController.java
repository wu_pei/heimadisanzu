package com.itheima.web;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Permission;
import com.itheima.service.AuthorityService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("authority")
public class AuthorityController {
    @Reference
    private AuthorityService authorityService;

    @RequestMapping("findAuthorities")
    public Result findAuthorities(@RequestBody QueryPageBean queryPageBean){
        try {
            PageResult pageResult = authorityService.findAuthorities(queryPageBean);
            return Result.success(MessageConstant.GET_AUTHORITY_LIST_SUCCESS,pageResult);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(MessageConstant.GET_AUTHORITY_LIST_FAIL);
        }
    }

    @RequestMapping("addAuthority")
    public Result addAuthority(@RequestBody Permission permission){
        boolean flag = authorityService.findPermissionByKeyword(permission.getKeyword());
        if (flag){
            try {
                authorityService.addPermission(permission);
                return Result.success(MessageConstant.ADD_AUTHORITY_SUCCESS);
            } catch (Exception e) {
                e.printStackTrace();
                return Result.error(MessageConstant.ADD_AUTHORITY_FAIL);
            }
        }else {
            return Result.error("已存在相同的keyword");
        }
    }

    @RequestMapping("delAuthorityById")
    public Result delAuthorityById(Integer id){
        boolean flag = authorityService.isBindPermission(id);
        if (flag){
            try {
                authorityService.delAuthorityById(id);
                return Result.success(MessageConstant.DELETE_AUTHORITY_SUCCESS);
            } catch (Exception e) {
                e.printStackTrace();
                return Result.error(MessageConstant.DELETE_AUTHORITY_FAIL);
            }
        }else {
            return Result.error(MessageConstant.DELETE_AUTHORITY_FAIL + ",已存在绑定关系");
        }
    }

    @RequestMapping("findAuthorityById")
    public Result findAuthorityById(Integer id){
        try {
            Permission permission = authorityService.findAuthorityById(id);
            return Result.success(MessageConstant.QUERY_AUTHORITY_SUCCESS,permission);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(MessageConstant.QUERY_AUTHORITY_FAIL);
        }
    }

    @RequestMapping("updateAuthorityById")
    public Result updateAuthorityById(@RequestBody Permission permission){
        try {
            authorityService.updateAuthorityById(permission);
            return Result.success(MessageConstant.EDIT_AUTHORITY_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(MessageConstant.EDIT_AUTHORITY_FAIL);
        }
    }
}
