package com.itheima.web;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.core.SystemLog;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.execption.CheckItemExecption;
import com.itheima.pojo.CheckItem;
import com.itheima.service.CheckItemService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/checkitem")
public class CheckItemController {
    @Reference
    CheckItemService checkItemService;

    /**
     * 页面传的是json数据，后端使用map 或者 pojo时 需要加@RequestBody
     * 基本类型 & 数组 & MultipartFile 只要保持页面的参数名称和controller方法形参一致就不用加@RequestParam
     * List 不管名字一不一样 必须加@RequestParam
     * @return
     */
    @RequestMapping("/add")
    @SystemLog(module = "checkitem",methods = "add")
    public Result add(@RequestBody CheckItem checkItem){
        try {
            checkItemService.add(checkItem);
            return new Result(true, MessageConstant.ADD_CHECKITEM_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_CHECKITEM_FAIL);
        }
    }

    @RequestMapping("/edit")
    @SystemLog(module = "checkitem",methods = "edit")
    public Result edit(@RequestBody CheckItem checkItem){
        checkItemService.edit(checkItem);
        return Result.success("编辑成分");
    }

    @RequestMapping("/findPage")
    @SystemLog(module = "checkitem",methods = "findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        return checkItemService.findPage(queryPageBean);
    }
    @RequestMapping("/findById")
    @SystemLog(module = "checkitem",methods = "findById")
    public Result findById(Integer id){
        CheckItem checkItem = checkItemService.findById(id);
        return Result.success("",checkItem);
    }

    @RequestMapping("/delete")

    @PreAuthorize("hasAuthority('CHECKITEM_DELETE')")
    @SystemLog(module = "checkitem",methods = "delete")
    public Result delete(Integer id){
        try {
            checkItemService.delete(id);
            return Result.success(MessageConstant.DELETE_CHECKITEM_SUCCESS);
        }catch (CheckItemExecption e){
            e.printStackTrace();
            return Result.error(e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            return Result.error(MessageConstant.DELETE_CHECKITEM_FAIL);
        }
    }
    @RequestMapping("/findAll")
    @SystemLog(module = "checkitem",methods = "findAll")
    public Result findAll(){
        List<CheckItem> checkItems = checkItemService.findAll();
        return Result.success("",checkItems);
    }

}
