package com.itheima.web;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.MyUser;
import com.itheima.service.UserService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    //jdk.nashorn.internal.ir.annotations 不要用这个
    @Reference
    UserService userService;

    @RequestMapping("/findUsers")
    public List<MyUser> findUsers(){
        return userService.findUsers();
    }

    @RequestMapping("/getUserName")
    public Result getUserName(){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return Result.success("",user.getUsername());
    }

    @RequestMapping("/getMenusAndPermissions")
    public Result getMenusAndPermissions(){
        Map map = userService.getMenusAndPermissions();
        return Result.success("",map);
    }

    @RequestMapping("addUser")
    public Result addUser(@RequestBody com.itheima.pojo.User user){
        if (!CollectionUtil.isNotEmpty(user.getRoleIds())){
            return Result.error("请勾选角色");
        }
        boolean flag =userService.findUserByUsername(user.getUsername());
        if (flag){
            try {
                userService.addUser(user);
                return Result.success("添加用户成功");
            } catch (Exception e) {
                e.printStackTrace();
                return Result.error("添加用户失败");
            }
        }else {
            return Result.error("用户名已存在");
        }

    }

    @RequestMapping("findUsersByCondition")
    public Result findUsersByCondition(@RequestBody QueryPageBean queryPageBean){
        try {
            PageResult pageResult = userService.findUsersByCondition(queryPageBean);
            return Result.success("查询用户成功",pageResult);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("查询用户失败");
        }
    }

    @RequestMapping("deleUserById")
    public Result deleUserById(Integer id){
        try {
            userService.deleUserById(id);
            return Result.success("删除用户成功");
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("删除用户失败");
        }
    }

    @RequestMapping("findUserById")
    public Result findUserById(Integer id){
        try {
            com.itheima.pojo.User user = userService.findUserById(id);
            return Result.success("查询用户成功",user);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("查询用户失败");
        }
    }

    @RequestMapping("updateUser")
    public Result updateUser(@RequestBody com.itheima.pojo.User user){
        try {
            userService.updateUser(user);
            return Result.success("更新成功");
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("更新失败");
        }
    }

}
