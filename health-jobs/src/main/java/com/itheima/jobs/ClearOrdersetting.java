package com.itheima.jobs;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;
import redis.clients.jedis.JedisPool;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author ZZH
 * data 2019.10.30 下午 4:27
 */
@Component
public class ClearOrdersetting {
    //设置数据库信息
    public void delete() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/health84");
        dataSource.setUsername("root");
        dataSource.setPassword("123456");

        //创建jdbcTemplate对象，设置数据源
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf.format(new Date());
        //调用jdbcTemplate对象的方法实现删除操作
        String sql = "DELETE FROM t_ordersetting WHERE orderDate<?";
        int rows = jdbcTemplate.update(sql,date);
        System.out.println(rows);

    }

}
