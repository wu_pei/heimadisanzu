package com.itheima.jobs;

import cn.hutool.core.collection.CollectionUtil;
import com.itheima.constant.RedisConstant;
import com.itheima.utils.QiniuUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.JedisPool;

import java.util.Date;
import java.util.Set;

@Component
public class ClearImages {
    @Autowired
    JedisPool jedisPool;

    public void doRun(){
       //比较两个集合得出的结果就是要删除的图片(集合里面数据多的放前面)
        Set<String> imgNames = jedisPool.getResource().sdiff(RedisConstant.SETMEAL_PIC_RESOURCES,
                RedisConstant.SETMEAL_PIC_DB_RESOURCES);
        if(CollectionUtil.isNotEmpty(imgNames)){
            for (String imgName : imgNames) {
                //删除七牛的图片
                QiniuUtil.delete(imgName);
                //避免下次比较做无用功所以删除SETMEAL_PIC_RESOURCES集合里面的垃圾图片
                jedisPool.getResource().srem(RedisConstant.SETMEAL_PIC_RESOURCES,imgName);
            }
        }
    }

}
