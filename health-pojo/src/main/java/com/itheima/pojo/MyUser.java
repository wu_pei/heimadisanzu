package com.itheima.pojo;

import java.io.Serializable;

//1.0 两个字段
//1.1 三个字段
public class MyUser implements Serializable {
    private Integer id;
    private String name;
    private Integer age;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
