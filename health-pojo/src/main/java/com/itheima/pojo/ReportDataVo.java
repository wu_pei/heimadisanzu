package com.itheima.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReportDataVo implements Serializable {
    private String reportDate;//2019-10-28
    private Integer todayNewMember;//今日新增会员数
    private Integer totalMember;//总会员数
    private Integer thisWeekNewMember;//本周新增会员数
    private Integer thisMonthNewMember;//本月新增会员数


    private Integer todayOrderNumber;//今日预约数
    private Integer todayVisitsNumber;//今日到诊数

    private Integer thisWeekOrderNumber;//本周预约数
    private Integer thisWeekVisitsNumber;//本周到诊数

    private Integer thisMonthOrderNumber;//本月预约数
    private Integer thisMonthVisitsNumber;//本月到诊数

    private List<HotSetmealVo> hotSetmeal;



}
