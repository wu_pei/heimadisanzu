package com.itheima.pojo;

import lombok.Data;

import java.io.Serializable;

@Data
public class LoginInfoVo implements Serializable {
    private String telephone;
    private String validateCode;

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getValidateCode() {
        return validateCode;
    }

    public void setValidateCode(String validateCode) {
        this.validateCode = validateCode;
    }
}
