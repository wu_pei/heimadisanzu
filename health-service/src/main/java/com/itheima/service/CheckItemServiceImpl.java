package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.CheckItemDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.execption.CheckItemExecption;
import com.itheima.pojo.CheckItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service(interfaceClass = CheckItemService.class)
@Transactional
public class CheckItemServiceImpl implements CheckItemService {

    @Autowired
    CheckItemDao checkItemDao;

    @Override
    public void add(CheckItem checkItem) {
        checkItemDao.add(checkItem);
    }

    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        //1、使用分页插件(打了一个标记，告诉PageInterceptor我们要分页查询)
        Page page = PageHelper.startPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        //2、执行查询
        List<CheckItem> checkItems = checkItemDao.findPage(queryPageBean.getQueryString());

        return new PageResult(page.getTotal(),checkItems);
    }

    @Override
    public void delete(Integer id) throws CheckItemExecption {
        //根据检查项的id去查询和检查组是否存在引用关系
        Integer count = checkItemDao.findCountByCheckItemId(id);
        if(null != count && count > 0){
            //存在引用关系
            throw new CheckItemExecption("存在引用关系,不能删除");
        } else {
            //执行删除语句
            checkItemDao.delete(id);
        }

    }

    @Override
    public CheckItem findById(Integer id) {
        return checkItemDao.findById(id);
    }

    @Override
    public void edit(CheckItem checkItem) {
        checkItemDao.edit(checkItem);
    }

    @Override
    public List<CheckItem> findAll() {

        return checkItemDao.findAll();
    }

    public static void main(String[] args) {
        Integer count = null;

        System.out.println( null != count);
        System.out.println( count != null);
    }
}
