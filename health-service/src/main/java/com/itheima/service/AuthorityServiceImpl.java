package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.AuthorityDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Permission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service(interfaceClass = AuthorityService.class)
@Transactional
public class AuthorityServiceImpl implements AuthorityService {
    @Autowired
    private AuthorityDao authorityDao;

    @Override
    public PageResult findAuthorities(QueryPageBean queryPageBean) {
        Page page = PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        List<Permission> permissions = authorityDao.findAuthoritiesByCondition(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(), permissions);
    }

    @Override
    public boolean findPermissionByKeyword(String keyword) {
        int count = authorityDao.findPermissionByKeyword(keyword);
        return count == 0;
    }

    @Override
    public void addPermission(Permission permission) {
        authorityDao.addPermission(permission);
    }

    @Override
    public boolean isBindPermission(Integer id) {
        int count = authorityDao.isBindPermission(id);
        return count == 0;
    }

    @Override
    public void delAuthorityById(Integer id) {
        authorityDao.delAuthorityById(id);
    }

    @Override
    public Permission findAuthorityById(Integer id) {
        return authorityDao.findAuthorityById(id);
    }

    @Override
    public void updateAuthorityById(Permission permission) {
        authorityDao.updateAuthorityById(permission);
    }
}
