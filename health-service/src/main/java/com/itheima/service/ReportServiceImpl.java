package com.itheima.service;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.MemberDao;
import com.itheima.dao.OrderDao;
import com.itheima.dao.ReportDao;
import com.itheima.pojo.Member;
import com.itheima.pojo.ReportDataVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = ReportService.class)
@Transactional
public class ReportServiceImpl implements ReportService {
    @Autowired
    ReportDao reportDao;
    @Autowired
    MemberDao memberDao;
    @Autowired
    OrderDao orderDao;
    @Override
    public List<Integer> getMemberReport(List<String> months) {
        List<Integer> memberCount = new ArrayList<>();
        for (String month : months) {
            Integer count = reportDao.getMemberReport(month);
            memberCount.add(count);
        }
        return memberCount;
    }

    @Override
    public List<Map> getSetmealReport() {
        return reportDao.getSetmealReport();
    }

    @Override
    public ReportDataVo getBusinessReportData() {
        String format = "yyyy-MM-dd";
        DateTime now = DateTime.now();//当前时间
        String nowStr = now.toString(format);
        String beginOfMonth = DateUtil.beginOfMonth(now).toString(format);//本月第一天
        String endOfMonth = DateUtil.endOfMonth(now).toString(format);//本月的最后一天

        String beginOfWeek = DateUtil.beginOfWeek(now).toString(format);//本周第一天
        String endOfWeek = DateUtil.endOfWeek(now).toString(format);//本周最后一天

//        -- 今日新增会员数
        Integer memberCountByDate = memberDao.findMemberCountByDate(nowStr);
//                -- 总会员数
        Integer memberTotalCount = memberDao.findMemberTotalCount();
//                -- 本周新增会员数（注册时间大于本周的第一天）
        Integer memberCountAfterDate = memberDao.findMemberCountAfterDate(beginOfWeek);
//                -- 本月新增会员数（注册时间大于本月第一天）
        Integer memberCountAfterDate1 = memberDao.findMemberCountAfterDate(beginOfMonth);
//                -- 今日预约数（统计预约时间是今天）
        Integer orderCountByDate = orderDao.findOrderCountByDate(nowStr);
//                -- 今日到诊数（统计预约时间是今天并且订单状态是已到诊）
        Integer visitsCountByDate = orderDao.findVisitsCountByDate(nowStr);
//                -- 本周预约数（预约时间在本周周一到本周周日之间）
        Integer orderCountBetween = orderDao.findOrderCountBetween(beginOfWeek,endOfWeek);
//                -- 本周到诊数（预约时间在本周周一到本周周日之间并且订单状态是已到诊）
        Integer orderVisitsCountBetween = orderDao.findVisitsOrderCountBetween(beginOfWeek,endOfWeek);
//                -- 本月预约数（预约时间在本月1号到本月最后一天）
        Integer orderCountBetween2 = orderDao.findOrderCountBetween(beginOfMonth,endOfMonth);
//                -- 本月到诊数（预约时间在本月1号到本月最后一天并且订单状态是已到诊）
        Integer orderVisitsCountBetween2 = orderDao.findVisitsOrderCountBetween(beginOfMonth,endOfMonth);

        ReportDataVo reportDataVo = ReportDataVo.builder()
                .reportDate(nowStr)
                .todayNewMember(memberCountByDate)
                .totalMember(memberTotalCount)
                .thisWeekNewMember(memberCountAfterDate)
                .thisMonthNewMember(memberCountAfterDate1)
                .todayOrderNumber(orderCountByDate)
                .todayVisitsNumber(visitsCountByDate)
                .thisWeekOrderNumber(orderCountBetween)
                .thisWeekVisitsNumber(orderVisitsCountBetween)
                .thisMonthOrderNumber(orderCountBetween2)
                .thisMonthVisitsNumber(orderVisitsCountBetween2)
                .hotSetmeal(orderDao.findHotSetmeal())
                .build();

        return reportDataVo;
    }

    @Override
    public List<List<Map>> getSexAndAgeReport() {
        //存放性别和年龄数据
        List<List<Map>> lists = new ArrayList<>();
        //存放性别数据
        List<Map> sexData = reportDao.getSexReport();
        for (Map sexDatum : sexData) {
            if (sexDatum.containsKey("name")) {
                System.out.println(sexDatum.get("name"));

                if ("1".equals(sexDatum.get("name"))) {
                    sexDatum.put("name","男");
                }
                if ("2".equals(sexDatum.get("name")) ) {
                    sexDatum.put("name", "女");
                }

            }
        }
        //存放年龄数据
        List<String> ageArr=reportDao.getAgeReport();
        //初始化计数器，记录各个年龄段的人数
        int ageCount1=0;
        int ageCount2=0;
        int ageCount3=0;
        int ageCount4=0;

        for (String str : ageArr) {
            //截取身份证字符串，得到年份值，进行分时间段判断
            String substr = str.substring(6, 10);
            if (Integer.valueOf(substr) <= 1974) {
                ageCount4++;
            } else if (Integer.valueOf(substr) > 1974 && Integer.valueOf(substr) <= 1989) {
                ageCount3++;
            } else if (Integer.valueOf(substr) > 1989 && Integer.valueOf(substr) <= 2001) {
                ageCount2++;
            } else {
                ageCount1++;
            }
        }
        //将年龄数据放入数组
        int[] arr = {ageCount1, ageCount2, ageCount3, ageCount4};
        //年龄对应的年龄段
        String[] arrStr = {"18以下", "18-30", "30-45", "45以上"};
        //创建存放年龄的集合
        List<Map> ageData = new ArrayList<>();

        for (int i = 0; i < arr.length; i++) {
            //将一个年龄段的放入一个map中，存入集合
            Map map = new LinkedHashMap();
            map.put("value", arr[i]);
            map.put("name", arrStr[i]);
            ageData.add(map);
        }
        lists.add(sexData);
        lists.add(ageData);
        return lists;
    }


    public static void main(String[] args) {


    }
}
