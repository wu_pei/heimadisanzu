package com.itheima.service;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.PermissionDao;
import com.itheima.dao.RoleDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service(interfaceClass = RoleService.class)
@Transactional
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleDao roleDao;

    @Autowired
    private PermissionDao permissionDao;

    @Override
    public Set<Role> findAllRoles() {
        return roleDao.findAllRoles();
    }

    @Override
    public Map findAllPermissionsAndMenus() {
        Set<Permission> permissions = permissionDao.findAllPermissions();

        Map map = new HashMap();
        map.put("permissions", permissions);
        return map;
    }

    @Override
    public PageResult findRolesByCondition(QueryPageBean queryPageBean) {
        Page page = PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        List<Role> roles = roleDao.findRolesByCondition(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(), roles);
    }

    @Override
    public boolean findRoleByKeyword(String keyword) {
        int count = roleDao.findRoleByKeyword(keyword);
        return count == 0;
    }

    @Override
    public void addRole(Role role) {
        roleDao.addRole(role);
        Integer id = role.getId();

        List<Integer> permissionIds = role.getPermissionIds();
        setRoleIdAndpermissionIds(id,permissionIds);

        List<Integer> menuIds = role.getMenuIds();
        setRoleIdAndMenuIds(id, menuIds);
    }

    @Override
    public void deleRoleById(Integer id) {
        roleDao.deleMenuIdByRoleId(id);
        roleDao.delePermissionIdByRoleId(id);

        roleDao.deleRoleById(id);
    }

    @Override
    public Role findRoleById(Integer id) {
        Role role = roleDao.findRoleById(id);

        Set<Permission> permissions = permissionDao.findAllPermissions();
        role.setPermissions(permissions);

        List<Integer> permissionIds = roleDao.findAllPermissionIds(id);
        role.setPermissionIds(permissionIds);
        List<Integer> menuIds = roleDao.findAllMenuIds(id);
        role.setMenuIds(menuIds);
        return role;
    }

    @Override
    public void updateRole(Role role) {
        roleDao.updateRole(role);

        Integer id = role.getId();
        roleDao.deleMenuIdByRoleId(id);
        roleDao.delePermissionIdByRoleId(id);

        List<Integer> permissionIds = role.getPermissionIds();
        setRoleIdAndpermissionIds(id, permissionIds);
        List<Integer> menuIds = role.getMenuIds();
        setRoleIdAndMenuIds(id,menuIds);

    }

    private void setRoleIdAndpermissionIds(Integer id, List<Integer> list){
        List<Map> maps = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(list)){
            for (Integer integer : list) {
                Map map = new HashMap();
                map.put("roleId", id);
                map.put("permissionIds", integer);
                maps.add(map);
            }
        }
        roleDao.addRoleIdAndpermissionIds(maps);
    }

    private void setRoleIdAndMenuIds(Integer id, List<Integer> list){
        List<Map> maps = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(list)){
            for (Integer integer : list) {
                Map map = new HashMap();
                map.put("roleId", id);
                map.put("MenuIds", integer);
                maps.add(map);
            }
        }
        roleDao.addRoleIdAndMenuIds(maps);
    }
}
