package com.itheima.service;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.CheckGroupDao;
import com.itheima.dao.CheckItemDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.CheckItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = CheckGroupService.class)
@Transactional
public class CheckGroupServiceImpl implements CheckGroupService {
    @Autowired
    CheckGroupDao checkGroupDao;
    @Autowired
    CheckItemDao checkItemDao;
    @Override
    public void add(CheckGroup checkGroup) {
        //第一步插入检查组基本信息并返回主键
        checkGroupDao.add(checkGroup);
        Integer checkGroupId = checkGroup.getId();
        //根据上一步返回的主键和页面用户勾选的检查项的id集合建立关系

        List<Integer> checkitemIds = checkGroup.getCheckitemIds();//页面用户勾选的检查项id集合
        setCheckGroupAndCheckItem(checkitemIds,checkGroupId);
    }

    private void setCheckGroupAndCheckItem(List<Integer> checkitemIds,Integer checkGroupId){
        if(CollectionUtil.isNotEmpty(checkitemIds)){
            //频繁和数据库交互
//            for (Integer checkitemId : checkitemIds) {
//                checkGroupDao.setCheckGroupAndCheckItem(checkitemId,checkGroupId);
//            }

            List<Map> params = new ArrayList<>();
            for (Integer checkitemId : checkitemIds) {
                Map map = new HashMap();
                map.put("checkGroupId",checkGroupId);
                map.put("checkitemId",checkitemId);
                params.add(map);
            }
            checkGroupDao.setCheckGroupAndCheckItemBatch(params);
        }
    }

    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        //告诉分页拦截器我们现在要分页
        Page page = PageHelper.startPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        checkGroupDao.findPage(queryPageBean.getQueryString());
//        Page<CheckGroup> page = checkGroupDao.findPage(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    public Map findById4Edit(Integer id) {
        Map result = new HashMap();
        //1、根据id查询检查组
        CheckGroup checkGroup = checkGroupDao.findById(id);
        result.put("checkGroup",checkGroup);
        //2、查询所有检查项
        List<CheckItem> checkItems = checkItemDao.findAll();
        result.put("checkItems",checkItems);
        //3、根据检查组id查询原来勾选的检查项的id集合
        List<Integer> checkItemIds = checkGroupDao.findCheckItemIdsByCheckGroupId(id);
        result.put("checkItemIds",checkItemIds);
        return result;
    }

    @Override
    public void edit(CheckGroup checkGroup) {
        //编辑检查组基本信息
        checkGroupDao.edit(checkGroup);
        //更新检查组和检查项原有的关系
           //根据检查组的id把原来的关系删除
        checkGroupDao.deleteAssociation(checkGroup.getId());
           //重新建立关系
        setCheckGroupAndCheckItem(checkGroup.getCheckitemIds(),checkGroup.getId());
    }

    @Override
    public List<CheckGroup> findAll() {
        return checkGroupDao.findAll();
    }
}
