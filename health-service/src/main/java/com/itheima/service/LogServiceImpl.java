package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.LogDao;
import com.itheima.entity.LogEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author hong
 */

@Service(interfaceClass = LogService.class)
@Transactional(rollbackFor = Exception.class)
public class LogServiceImpl implements LogService {

    @Autowired
    LogDao logDao;

    @Override
    public void saveLog(LogEntity log) {
        logDao.saveLog(log);
    }
}
