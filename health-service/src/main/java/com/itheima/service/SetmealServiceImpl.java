package com.itheima.service;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.SetmealDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.CheckItem;
import com.itheima.pojo.Setmeal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service(interfaceClass = SetmealService.class)
@Transactional
public class SetmealServiceImpl implements SetmealService {
    @Autowired
    SetmealDao setmealDao;
    @Override
    public void add(Setmeal setmeal) {
        //保存套餐基本信息并返回主键
        setmealDao.add(setmeal);
        Integer setmealId = setmeal.getId();
        //循环用户勾选检查组和套餐的id建立关系
        List<Integer> checkgroupIds = setmeal.getCheckgroupIds();
        if(CollectionUtil.isNotEmpty(checkgroupIds)){
//            for (Integer checkgroupId : checkgroupIds) {
//                setmealDao.setSetmealAndCheckGroup(checkgroupId,setmealId);
//            }


            List<Map> params = new ArrayList<>();
            for (Integer checkgroupId : checkgroupIds) {
                Map map = new HashMap();
                map.put("checkgroupId",checkgroupId);
                map.put("setmealId",setmealId);
                params.add(map);
            }
            setmealDao.setSetmealAndCheckGroupBatch(params);
        }
    }

    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        Page page = PageHelper.startPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        setmealDao.findPage(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(),page.getResult());

    }

    @Override
    public List<Setmeal> findAll() {
        return setmealDao.findAll();
    }

    @Override
    public List<Setmeal> getSetmeal() {
        return setmealDao.getSetmeal();
    }

    @Override
    public Setmeal findById(Integer id) {//id 套餐id
        //根据套餐的id查询套餐的基本信息  select * from t_setmeal where id = 5
        Setmeal setmeal = setmealDao.findById(id);
        if(null != setmeal){
            //根据套餐id查询套餐下面的检查组基本信息集合 select * from t_checkgroup where id in (select checkgroup_id from t_setmeal_checkgroup where setmeal_id = 5)
          List<CheckGroup> checkGroups =  setmealDao.findCheckGroupsBySetmealId(id);
            if(CollectionUtil.isNotEmpty(checkGroups)){
                //for 检查组基本信息集合 一个一个查询检查组下面的检查项基本信息集合 select * from t_checkitem where id in (select checkitem_id from t_checkgroup_checkitem where checkgroup_id = 5)
                for (CheckGroup checkGroup : checkGroups) {
                    List<CheckItem> checkItems = setmealDao.findCheckItemsByCheckGroupId(checkGroup.getId());
                    checkGroup.setCheckItems(checkItems);
                }
            }
          setmeal.setCheckGroups(checkGroups);
        }
        return setmeal;
    }

    @Override
    public Setmeal findByIdBatch(Integer id) {
        Setmeal setmeal = setmealDao.findById(id);
        if(null != setmeal){
            List<CheckGroup> checkGroups =  setmealDao.findCheckGroupsBySetmealId(id);
            if(CollectionUtil.isNotEmpty(checkGroups)){
                //一次性把根据checkGroupIds把所有的检查项查询出来
                List<Integer> ids = getCheckGroupIds(checkGroups);
                List<CheckItem> checkItems = setmealDao.findCheckItemsByCheckGroupIdBatch(ids);
                //把查询出来的所有检查项进行分组
//                Map<Integer,List<CheckItem>> result = checkItemGroupBy(checkItems);
                Map<Integer, List<CheckItem>> result = checkItems.stream().
                        collect(Collectors.groupingBy(CheckItem::getCheckGroupId));

                for (CheckGroup checkGroup : checkGroups) {
                    checkGroup.setCheckItems(result.get(checkGroup.getId()));
                }
            }
            setmeal.setCheckGroups(checkGroups);
        }
        return setmeal;
    }

    @Override
    public Setmeal findDetailById(Integer id) {
        return setmealDao.findById(id);
    }

    private Map<Integer,List<CheckItem>> checkItemGroupBy(List<CheckItem> checkItems) {
        Map<Integer,List<CheckItem>> result = new HashMap<>();
        for (CheckItem checkItem : checkItems) {
            //检查组的id
            Integer checkGroupId = checkItem.getCheckGroupId();
            //根据检查组id去看map里面是否有坑
            List<CheckItem> list = result.get(checkGroupId);
            if(null == list){
                //没有坑就造一个坑
                list = new ArrayList<>();
                result.put(checkGroupId,list);
            }
            //直接填坑
            list.add(checkItem);
        }
        return result;
    }

    private List<Integer> getCheckGroupIds(List<CheckGroup> checkGroups) {
        List<Integer> ids = new ArrayList<>();
        for (CheckGroup checkGroup : checkGroups) {
            ids.add(checkGroup.getId());
        }
        return ids;
    }
}
