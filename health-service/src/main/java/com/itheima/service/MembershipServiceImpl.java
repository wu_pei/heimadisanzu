package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.MembershipMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Service(interfaceClass =MembershipService.class )
public class MembershipServiceImpl implements MembershipService{
    @Autowired
    private MembershipMapper mapper;

    @Override
    public List<Integer> findMembersByDateRange(List<String> months) {
        //一个月一月的查
        List<Integer> list = new ArrayList<>();
        for (String month : months) {
            Integer count = mapper.findMembersByDateRange(month);
            list.add(count);
        }
        return list;
    }
}
