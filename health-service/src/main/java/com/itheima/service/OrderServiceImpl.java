package com.itheima.service;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.MemberDao;
import com.itheima.dao.OrderDao;
import com.itheima.dao.OrderSettingDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Member;
import com.itheima.pojo.Order;
import com.itheima.pojo.OrderInfoVo;
import com.itheima.pojo.OrderSetting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = OrderService.class)
@Transactional
public class OrderServiceImpl implements OrderService {
    @Autowired
    OrderDao orderDao;
    @Autowired
    OrderSettingDao orderSettingDao;
    @Autowired
    MemberDao memberDao;

    @Override
    public Result submit(OrderInfoVo orderInfoVo) {
        Date orderDate = orderInfoVo.getOrderDate();
        String telephone = orderInfoVo.getTelephone();
        String idCard = orderInfoVo.getIdCard();
        String name = orderInfoVo.getName();
        Integer setmealId = orderInfoVo.getSetmealId();
        Integer sex = orderInfoVo.getSex();
        String orderType = orderInfoVo.getOrderType();

//        1.3.2、根据用户选择的预约日期判断当前是否存在预约设置
        OrderSetting orderSetting = orderSettingDao.findOrderSettingByDate(orderDate);
        //线程1 1455	2019-10-27	105	104	0
        //线程2 1455	2019-10-27	105	104	0
        //线程3 1455	2019-10-27	105	104	0
        //线程4 1455	2019-10-27	105	104	0
        //线程5 1455	2019-10-27	105	104	0
        //线程6 1455	2019-10-27	105	104	0
        //线程7 1455	2019-10-27	105	104	0
        //线程8 1455	2019-10-27	105	104	0
        if(null == orderSetting){
            return Result.error("没有档期");
        }
//        1.3.3、如果存在预约设置就需要判断是否约满
        int number = orderSetting.getNumber();//可预约人数
        int reservations = orderSetting.getReservations();//已预约人数
//        线程1  number=105 ，reservations=104
//        线程2  number=105 ，reservations=104
//        线程3  number=105 ，reservations=104
//        线程4  number=105 ，reservations=104
//        线程5  number=105 ，reservations=104
//        线程6  number=105 ，reservations=104
//        线程7  number=105 ，reservations=104
//        线程8  number=105 ，reservations=104
//        线程9  number=105 ，reservations=104
//        线程10  number=105 ，reservations=104
//        线程11  number=105 ，reservations=104
        if(number <= reservations){
            return Result.error("预约已满");
        }
//        1.3.4、根据用户手机号码检查系统是否存在账号信息（并返回userid）
        Member member = memberDao.findByTelephone(telephone);
        if(null == member){
            //需要帮他注册
            member = new Member();
            member.setName(name);
            member.setPhoneNumber(telephone);
            member.setSex(String.valueOf(sex));
            member.setIdCard(idCard);
            member.setRegTime(new Date());
            memberDao.add(member);
        }
        Integer memberId = member.getId();
//        1.3.5、根据用户id，套餐id，预约时间判断是否存在预约
        Order queryParam = new Order();
        queryParam.setSetmealId(setmealId);
        queryParam.setMemberId(memberId);
        queryParam.setOrderDate(orderDate);
        List<Order> orders = orderDao.findByCondition(queryParam);
        if(CollectionUtil.isNotEmpty(orders)){
            return Result.error("已经存在预约，不要重复预约");
        }
//        1.3.6、创建预约订单
        Order order = new Order();
        order.setMemberId(memberId);
        order.setOrderDate(orderDate);
        order.setOrderType(orderType);
        order.setOrderStatus(Order.ORDERSTATUS_NO);
        order.setSetmealId(setmealId);
        orderDao.add(order);
//        1.3.7、修改已预约人数（+1）

        orderSetting.setReservations(orderSetting.getReservations() + 1);
        int row = orderSettingDao.updateReservations(orderSetting);//数据库有行锁
        if(1 != row){
            throw  new RuntimeException("更新失败");
        }
        //线程1 1455	2019-10-27	105	104	1
        //线程2 1455	2019-10-27	105	104	0
        //线程3 1455	2019-10-27	105	104	0
//        1.3.8、返回预约成功(页面跳转需要orderId)
        return Result.success("预约成功",order);
    }

    @Override
    public Map findById(Integer id) {
        return orderDao.findById4Detail(id);
    }

    @Override

    public List<Member> findFamilyById(Integer id) {

        List<Member> members = orderDao.findFamilyById(id);

        return members;
    }

    @Override
    public Result addFamily(Member member1) {
        int count = orderDao.findFamily(member1);
        if (0 == count) {
            orderDao.addFamily(member1);
            return new Result(true, "添加联系人成功");
        } else {
            return new Result(false, "已经存在，请勿重复添加");
        }


    }

    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        //分页信号
        Page page = PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        //开始查询
        List<OrderInfoVo> orderInfoVos = orderDao.findPage(queryPageBean.getQueryString());
        //返回PageResult，参数1为:总记录数，参数2为:当前页结果
        return new PageResult(page.getTotal(), orderInfoVos);
    }
}
