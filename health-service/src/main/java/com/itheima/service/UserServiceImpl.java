package com.itheima.service;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.PermissionDao;
import com.itheima.dao.RoleDao;
import com.itheima.dao.UserDao;
import com.itheima.dao.UserMapper;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service(interfaceClass = UserService.class)
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserDao userDao;

    @Autowired
    RoleDao roleDao;

    @Autowired
    PermissionDao permissionDao;

    @Override
    public List<MyUser> findUsers() {
        return null;
    }

    @Override
    public User findUserByUserName(String username) {
        //根据用户名查询用户基本信息
        User user =  userMapper.findByUserName(username);
        if(null != user){
            //根据用户id查询用户角色
            Set<Role> roles = roleDao.findByUserId(user.getId());
            if(CollectionUtil.isNotEmpty(roles)){
                //根据角色ids循环查询角色下面对应的权限集合
//                for (Role role : roles) {
//                    Set<Permission> permissions = permissionDao.findByRoleId(role.getId());
//                    role.setPermissions(permissions);
//                }
//                user.setRoles(roles);

                List<Integer> ids =  getRolds(roles);
                Set<Permission> permissions = permissionDao.findByRoleIds(ids);
                user.setPermissions(permissions);
            }
        }
        return user;

    }

    @Override
    public Map getMenusAndPermissions() {
        Set<Permission> permissions = permissionDao.findAll();
        //先查询一级菜单
       List<Menu> menus =  permissionDao.getFirstMenus();
        for (Menu menu : menus) {
            menu.setChildren(permissionDao.getSecondMenus(menu.getId()));
        }
        Map map = new HashMap();
        map.put("permissions",permissions);
        map.put("menus",menus);
        return map;
    }

    private List<Integer> getRolds(Set<Role> roles) {
        List<Integer> ids = new ArrayList<>();
        for (Role role : roles) {
            ids.add(role.getId());
        }
        return ids;
    }

    @Override
    public PageResult findUsersByCondition(QueryPageBean queryPageBean) {
        Page page = PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        List<User> users = userDao.findUserByCondition(queryPageBean.getQueryString());
        for (User user : users) {
            if (user.getPassword() != null){
                user.setPassword("******");
            }
        }
        return new PageResult(page.getTotal(), users);
    }

    @Override
    public boolean findUserByUsername(String username) {
        int count = userDao.findUserByUsername(username);
        return count == 0;
    }

    @Override
    public void addUser(User user) {
        BCryptPasswordEncoder encode = new BCryptPasswordEncoder();
        user.setPassword(encode.encode(user.getPassword()));

        userDao.addUser(user);
        Integer id = user.getId();

        List<Integer> roleIds = user.getRoleIds();
        setCheckItemIdAndCheckGroupId(id, roleIds);
    }

    @Override
    public void deleUserById(Integer id) {
        userDao.deleRoleIdByUserId(id);
        userDao.deleUserById(id);

    }

    @Override
    public User findUserById(Integer id) {
        User user = userDao.findUserById(id);

        Set<Role> roles = roleDao.findAllRoles();
        user.setRoles(roles);

        List<Integer> roleIds = userDao.findRoleIdsByUserId(id);
        user.setRoleIds(roleIds);
        return user;
    }

    @Override
    public void updateUser(User user) {
        userDao.updateUser(user);

        Integer id = user.getId();
        userDao.deleRoleIdByUserId(id);

        List<Integer> roleIds = user.getRoleIds();
        setCheckItemIdAndCheckGroupId(id, roleIds);
    }

    private void setCheckItemIdAndCheckGroupId(Integer id,List<Integer> list){
        List<Map> maps = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(list)){
            for (Integer integer : list) {
                Map map = new HashMap();
                map.put("userId", id);
                map.put("roleId", integer);
                maps.add(map);
            }
        }
        userDao.addUserIdAndRoleId(maps);
    }
}
