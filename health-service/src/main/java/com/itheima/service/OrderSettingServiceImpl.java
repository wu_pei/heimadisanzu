package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.itheima.dao.OrderSettingDao;
import com.itheima.pojo.OrderSetting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

@Service(interfaceClass = OrderSettingService.class)
@Transactional
public class OrderSettingServiceImpl implements OrderSettingService {
    @Autowired
    OrderSettingDao orderSettingDao;

    @Override
    public void setNumberByDate(OrderSetting orderSetting) {
        //根据预约设置的日期查询一下是否存在预约设置
        Integer count = orderSettingDao.findOrderSettingCount(orderSetting.getOrderDate());
        if(null != count && count > 0){
            //存在就更新
            orderSettingDao.update(orderSetting);
        } else {
            //不存在就插入
            orderSettingDao.add(orderSetting);
        }


    }

    @Override
    public void batchAdd(List<OrderSetting> orderSettings) {
        long start = System.currentTimeMillis();

//        for (OrderSetting orderSetting : orderSettings) {
//            setNumberByDate(orderSetting);
//        }





        //固定创建5个线程
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);

        //多线程
//        orderSettings集合分成多份，

        //把集合拆分每份200数据
        List<List<OrderSetting>> partition = Lists.partition(orderSettings, 400);

        List<Future<Integer>> futures = new ArrayList<>();
        //我们给每个线程飞一份数据去执行
//        Thread
//        Runnable  没返回值
//        Callable 有返回值
        for (List<OrderSetting> settings : partition) {
            Future<Integer> future = executorService.submit(new Callable<Integer>() {
                @Override
                public Integer call() throws Exception {
                    for (OrderSetting setting : settings) {
                        setNumberByDate(setting);
                    }
                    return 1;
                }
            });
            //把票拿在手里
            futures.add(future);
        }

        int count = 0;

        for (Future<Integer> future : futures) {
            try {
                Integer o = future.get();//如果线程没有干完会阻塞
                count = count + o;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }


        if(count == partition.size()){
            //告诉客户端已大帮人帮你把数据批量导入到数据库
            System.out.println(partition.size());
        }


        long end = System.currentTimeMillis();

        System.out.println("做完用了时间：" + (end - start));

    }

    @Override
    public List<OrderSetting> findOrderSettingsByMonth(String month) {//2019-10
        //select * from t_ordersetting where orderDate BETWEEN '2019-09-01' AND '2019-09-31'
        String start = month + "-01";
        String end = month + "-31";
        return orderSettingDao.findOrderSettingsByMonth(start,end);
    }
}
