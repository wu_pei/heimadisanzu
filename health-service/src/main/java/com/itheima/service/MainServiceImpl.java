package com.itheima.service;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.MenuDao;
import com.itheima.pojo.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service(interfaceClass = MainService.class)
@Transactional
public class MainServiceImpl implements MainService {
    @Autowired
    private MenuDao menuDao;


    @Override
    public List<Menu> getMenus(String userName) {
        /*//1.通过用户名查询到该用户所有菜单id
        List<Integer> menuIds = menuDao.getAllMenuIdByUserName(userName);*/
        //查询到该用户所有的菜单功能
        List<Menu> menus = menuDao.getAllMenu(userName);

        //查询出一级菜单
        List<Menu> oneMenus = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(menus)) {
            for (Menu menu : menus) {
                if (menu.getParentMenuId() == null) {
                    menu.setLabel(menu.getName());
                    menu.setIcon(menu.getIcon());
                    menu.setPath(menu.getPath());
                    oneMenus.add(menu);
                }
            }
        }
//        使用递归，查询子菜单
        for (Menu menu : menus) {
            List<Menu> childList = getChildren(String.valueOf(menu.getId()), menus);
            menu.setChildren(childList);
        }

        return oneMenus;
    }

    private List<Menu> getChildren(String id, List<Menu> menus) {
//        构建子菜单集合
        List<Menu> childList = new ArrayList<>();

//        遍历传过来的菜单，若是父菜单Id等于传过来的id，则为该子菜单
        if (CollectionUtil.isNotEmpty(menus)) {
            for (Menu menu : menus) {
                if (String.valueOf(menu.getParentMenuId()).equals(id)) {
                    childList.add(menu);
                }
            }
        }

//        递归调用
        for (Menu menu : childList) {
            menu.setChildren(getChildren(String.valueOf(menu.getId()), menus));
        }

//        结束标志
        if (childList.size() == 0) {
            return null;
        }

        return childList;
    }
}
