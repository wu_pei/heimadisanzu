package com.itheima.service;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.MenuDao;
import com.itheima.pojo.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service(interfaceClass = MenuService.class)
@Transactional
public class MenuServiceImpl implements MenuService {
    @Autowired
    private MenuDao menuDao;

    @Override
    public List<Menu> getAllMenus() {
//        首先查询所有菜单
        List<Menu> menus = menuDao.getAllMenus();

//        查询出一级菜单
        List<Menu> oneMenus = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(menus)){
            for (Menu menu : menus) {
                if (menu.getParentMenuId() == null){
                    menu.setLabel(menu.getName());
                    oneMenus.add(menu);
                }
            }
        }
//        使用递归，查询子菜单
        for (Menu menu : menus) {
            List<Menu> childList = getChildren(String.valueOf(menu.getId()),menus);
            menu.setChildren(childList);
        }

        return oneMenus;
    }

    @Override
    public void addOneMenu(Menu menu) {
        if (menu.getId() == null){
            menuDao.addOneMenu(menu);
        }else {
            menu.setParentMenuId(menu.getId());
            menuDao.addOneMenu(menu);
        }
    }

    @Override
    public void delMenuById(Integer id) {
//        首先查出该级菜单下的子级菜单集合;
        List<Menu> menus = menuDao.findChileMenusByParentId(id);

        if (CollectionUtil.isNotEmpty(menus)){
            for (Menu child : menus) {
                delChildren(child);
            }
        }

        menuDao.delMenuById(id);
    }

    @Override
    public boolean isBindMenu(Integer id) {
        int count = menuDao.isBindMenu(id);
        return count == 0;
    }

    @Override
    public Menu findMenuById(Integer id) {
        return menuDao.findMenuById(id);
    }

    @Override
    public void updateMenuById(Menu menu) {
        menuDao.updateMenuById(menu);
    }

    private void delChildren(Menu menu){
        List<Menu> children = menuDao.findChileMenusByParentId(menu.getId());
        if (CollectionUtil.isNotEmpty(children)) {
            for (Menu child : children) {
                delChildren(child);
            }
        }
        menuDao.delMenuById(menu.getId());
    }

    private List<Menu> getChildren(String id, List<Menu> menus){
//        构建子菜单集合
        List<Menu> childList = new ArrayList<>();

//        遍历传过来的菜单，若是父菜单Id等于传过来的id，则为该子菜单
        if (CollectionUtil.isNotEmpty(menus)){
            for (Menu menu : menus) {
                if (String.valueOf(menu.getParentMenuId()).equals(id)){
                    menu.setLabel(menu.getName());
                    childList.add(menu);
                }
            }
        }

//        递归调用
        for (Menu menu : childList) {
            menu.setChildren(getChildren(String.valueOf(menu.getId()), menus));
        }

//        结束标志
        if (childList.size() == 0){
            return null;
        }

        return childList;
    }
}
