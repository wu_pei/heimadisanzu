package com.itheima.dao;

import com.itheima.pojo.Menu;
import com.itheima.pojo.Permission;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

public interface PermissionDao {
    Set<Permission> findByRoleId(@Param("id") Integer id);

    Set<Permission> findByRoleIds(@Param("ids") List<Integer> ids);

    Set<Permission> findAll();

    List<Menu> getFirstMenus();

    List<Menu> getSecondMenus(@Param("id") Integer id);

    Set<Permission> findAllPermissions();
}
