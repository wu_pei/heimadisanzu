package com.itheima.dao;

import com.itheima.entity.LogEntity;

/**
 * @author hong
 */
public interface LogDao {
    void saveLog(LogEntity log);
}
