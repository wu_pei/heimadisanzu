package com.itheima.dao;

import com.itheima.pojo.Menu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MenuDao {
    List<Integer> getAllMenuIdByUserName(@Param("userName") String userName);

    List<Menu> getAllMenu(@Param("userName") String userName);

    List<Menu> getAllMenus();

    void addOneMenu(Menu menu);

    int isBindMenu(@Param("id") Integer id);

    List<Menu> findChileMenusByParentId(@Param("id") Integer id);

    void delMenuById(@Param("id") Integer id);

    Menu findMenuById(@Param("id") Integer id);

    void updateMenuById(Menu menu);

}
