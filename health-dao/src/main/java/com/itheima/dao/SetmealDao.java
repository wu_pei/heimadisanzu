package com.itheima.dao;

import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.CheckItem;
import com.itheima.pojo.Setmeal;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SetmealDao {
    void add(Setmeal setmeal);

    void setSetmealAndCheckGroup(@Param("checkgroupId") Integer checkgroupId,@Param("setmealId") Integer setmealId);

    void setSetmealAndCheckGroupBatch(List<Map> params);

    List<Setmeal> findPage(@Param("queryString")String queryString);

    List<Setmeal> getSetmeal();

    Setmeal findById(@Param("id") Integer id);

    List<CheckGroup> findCheckGroupsBySetmealId(@Param("id")Integer id);

    List<CheckItem> findCheckItemsByCheckGroupId(@Param("id") Integer id);

    List<CheckItem> findCheckItemsByCheckGroupIdBatch(@Param("ids") List<Integer> ids);

    List<Setmeal> findAll();
}
