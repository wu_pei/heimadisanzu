package com.itheima.dao;

import com.itheima.pojo.HotSetmealVo;
import com.itheima.pojo.Member;
import com.itheima.pojo.Order;
import com.itheima.pojo.OrderInfoVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface OrderDao {
    public void add(Order order);
    public List<Order> findByCondition(Order order);
    public Map findById4Detail(Integer id);
    public Integer findOrderCountByDate(String date);
    public Integer findOrderCountAfterDate(String date);
    public Integer findVisitsCountByDate(String date);
    public Integer findVisitsCountAfterDate(String date);
    public List<HotSetmealVo> findHotSetmeal();

    Integer findOrderCountBetween(@Param("start") String beginOfWeek, @Param("end") String endOfWeek);

    Integer findVisitsOrderCountBetween(@Param("start") String beginOfWeek, @Param("end") String endOfWeek);


    List<Member> findFamilyById(@Param("id") Integer id);

    int findFamily(Member member1);


    void addFamily(Member member1);

    List<OrderInfoVo> findPage(@Param("queryString")String queryString);

}
