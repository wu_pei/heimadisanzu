package com.itheima.dao;

import com.itheima.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface UserDao {
    List<User> findUserByCondition(@Param("queryString") String queryString);

    User findUserByUserName(@Param("username") String username);

    int findUserByUsername(@Param("username") String username);

    void addUser(User user);

    void addUserIdAndRoleId(List<Map> maps);

    void deleRoleIdByUserId(@Param("id") Integer id);

    void deleUserById(@Param("id") Integer id);

    User findUserById(@Param("id") Integer id);

    List<Integer> findRoleIdsByUserId(@Param("id") Integer id);

    void updateUser(User user);
}
