package com.itheima.dao;

import com.itheima.pojo.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface RoleDao {
    Set<Role> findByUserId(@Param("id") Integer id);

    Set<Role> findAllRoles();

    List<Role> findRolesByCondition(@Param("queryString") String queryString);

    int findRoleByKeyword(@Param("keyword") String keyword);

    void addRole(Role role);

    void addRoleIdAndpermissionIds(List<Map> maps);

    void addRoleIdAndMenuIds(List<Map> maps);

    void deleMenuIdByRoleId(@Param("id") Integer id);

    void delePermissionIdByRoleId(@Param("id") Integer id);

    void deleRoleById(@Param("id") Integer id);

    Role findRoleById(@Param("id") Integer id);

    List<Integer> findAllPermissionIds(@Param("id") Integer id);

    List<Integer> findAllMenuIds(@Param("id") Integer id);

    void updateRole(Role role);
}
