package com.itheima.dao;

import org.apache.ibatis.annotations.Param;


public interface MembershipMapper {
    Integer findMembersByDateRange(@Param("month") String month);
}
