package com.itheima.dao;

import com.itheima.pojo.Permission;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AuthorityDao {
    List<Permission> findAuthoritiesByCondition(@Param("queryString") String queryString);

    int findPermissionByKeyword(@Param("keyword") String keyword);

    void addPermission(Permission permission);

    int isBindPermission(@Param("id") Integer id);

    void delAuthorityById(@Param("id") Integer id);

    Permission findAuthorityById(@Param("id") Integer id);

    void updateAuthorityById(Permission permission);
}
